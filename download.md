---
layout: page
title: Download
css-include: /css/download.css
sorted: 3

sources:
  - name: Linux
    icon: /assets/img/tux.png
    description: >
        Konversation is already available on the majority of Linux distributions.
        You can install it from <a href="appstream://org.kde.konversation.desktop">Discover</a> or
        <a href="appstream://org.kde.konversation.desktop">GNOME Software</a>.
  - name: Release Sources
    icon: /assets/img/ark.svg
    description: >
        You can find Konversation latest stable release as a
        <a href="https://download.kde.org/stable/konversation/">tarballs</a>.

        If you want to build Konversation from source, we recommend checking out our
        <a href="get-involved.html">Getting Involved</a> page, which contains links to
        a full guide how to compile Konversation yourself.
  - name: Git
    icon: /assets/img/git.svg
    description: >
        Konversation git repository can be viewed
        <a href="https://invent.kde.org/network/konversation">using KDE's GitLab instance</a>.

        To clone Konversation, use <code>git clone
        https://invent.kde.org/network/konversation.git</code>. For detailed instructions on how
        to build Konversation from source, check the <a href="https://community.kde.org/Get_Involved/development">Getting
        Involved page</a>
  # - name: Windows
  #   icon: /assets/img/windows.svg
  #   description: >
  #       Konversation is also available for Windows, both as
  #       <a href="https://binary-factory.kde.org/job/Konversation_Release_win64/">stable version</a> and
  #       <a href="https://binary-factory.kde.org/job/Konversation_Nightly_win64/">latest development version</a>.

---

<h1>Download</h1>

<table class="distribution-table">
  {% for source in page.sources %}
    <tr class="title-row">
      <td rowspan="2" width="100">
        <img src="{{ source.icon }}" alt="{{ source.name }}">
      </td>
      <th>{{ source.name }}</th>
    </tr>
    <tr>
      <td>{{ source.description }}</td>
    </tr>
  {% endfor %}
</table>
