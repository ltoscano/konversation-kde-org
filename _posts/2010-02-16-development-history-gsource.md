---
title: The development history of Konversation visualized using gource 
date: 2010-02-16
layout: post
---
As a little change of pace from the usual release announcement postings, here\'s a short visual treat: The source code revision history of your favorite IRC client, visualized using a nifty tool called [gource](http://code.google.com/p/gource/). [Check out](http://www.youtube.com/watch?v=gJiHGc8qPt4&amp;hd=1) on YouTube, or better yet fetch the high-quality Ogg Theora version from [here](http://konversation.kde.org/downloads/konversation_gource-2010-02-16.ogg"). 
(1280x720 143 MB).
