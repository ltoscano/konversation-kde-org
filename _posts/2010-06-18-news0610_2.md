---
title: konversation's Git repository has moved  
date: 2010-06-18
layout: post
---
Back in December 2009, Konversation made the move from Subversion to [Git](http://git-scm.com/) as its version control solution. In the course of that, our source code moved from KDE's Subversion server to the [Gitorious.org](http://www.gitorious.org) platform. Konversation was the second KDE project after [Amarok](http://amarok.kde.org/) to do so; the goal was to test the waters for the eventual migration of KDE as a whole. 

 Recently, however, KDE's Git migration has changed course a bit: Instead of moving to Gitorious.org, we have decided to host our Git repositories within our own infrastructure (you can read more about this decision and the details of the new setup [here](http://mail.kde.org/pipermail/kde-scm-interest/2010-June/001438.html) and [here](http://mail.kde.org/pipermail/konversation-devel/2010-June/003542.html). For the projects that already moved to Gitorious.org - by now, more than a dozen - this means a second move, and again Amarok and Konversation have moved first to help prepare the ground. 

 Our [wiki](http://konversation.kde.org/wiki) has already been updated, but let's go over the new URLs / commands right here: 

 
+ Repository browsing: [http://projects.kde.org/projects/konversation/repository](http://projects.kde.org/projects/konversation/repository) ([Redmine](http://www.redmine.org/) and [http://cgit.kde.org/konversation/konversation/](http://cgit.kde.org/konversation/konversation/) (using cgit) 
+ Read-only Git access: `git clone git://git.kde.org/konversation/konversation.git`
+ Read-write Git access (for developers): `git clone git@git.kde.org:konversation/konversation.git` 
 

 (Note: If you already have a clone, you don't have to clone again. Rather, you can use `git remote set-url origin < \new url >\` to switch over to the new URL.) 

 Commit access to the repository on Gitorious.org has been revoked, and once we've dealt with the in-flight merge requests still found there, the project on Gitorious.org will be deleted. 


