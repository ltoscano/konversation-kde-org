---
title: Konversation now handled by KDE Release Service
date: 2020-12-10
layout: post
---

Starting with [Release Service 20.12](https://kde.org/announcements/releases/2020-12-apps-update/), new releases of Konversation are handled by the team behind KDE's Release Service (*Update*: since 21.04 named KDE Gear).

This change allows to concentrate the currently available developer resources on the source code while ensuring a timely delivery of improvements and bug fixes to users.
For distribution packagers the now aligned release schedule allows better planning and more automated processing.

So please check the [Release Service announcements](https://kde.org/announcements) (*Update*: since 21.04 [KDE Gear announcements](https://kde.org/announcements)) for details about new Konversation releases and any new features and bug fixes.
