---
title: Konversation 1.3 has been released!
date: 2010-06-07
layout: post
---

Konversation 1.3 debuts a major new feature in the area of Direct-Client-to-Client (DCC) support: An implementation of the DCC Whiteboard extension that brings collaborative drawing - think two-player Kolourpaint - to IRC. It also brings back the integration with KDE's SSL certificate store the KDE 3 version enjoyed and expands support for auto-away to the Windows and Mac OS X platforms thanks to both recent advances in the KDE 4 platform and new code in Konversation. Interface tweaks, new keyboard shortcuts and many bugfixes (including a number of new fixes since 1.3-beta1) round things out. Finally, Konversation now depends on KDE 4.3 and Qt 4.5. 

 Changes from 1.3-beta1 to 1.3: 
 
+ Fixed build with KDE 4.3. 
+ When opening an "Edit Network" dialog and adding a new item to one of the server or channel lists, provided they already contain at least one item and no selection is made before clicking "Add...", the "Move Down" button would be enabled afterwards despite no item being selected. Clicking the button at this point would crash the application. This has been fixed along with other potential problems in the code that updates the state of the list control buttons. 
+ After adding a new item to one of the server or channel lists in "Edit Network" dialogs, that item will now be selected. 
+ Fixed a bug causing the file dialog for selecting a new target directory and file name for an incoming DCC file transfer in the event that the default path is not writable to complain about being unable to find the file after clicking "OK" when no file of the chosen name at the chosen location exists already. 
+ Fixed a bug causing the file dialog for selecting a new target directory and file name for an incoming DCC file transfer in the event that the default path is not writable to lose the file name written in the "Location" field (by default, the original file name) when changing the current directory. 
 


