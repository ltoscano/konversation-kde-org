---
title: Konversation 1.0 has been released! 
date: 2006-08-31
layout: post
---

We are extremely pleased to announce the immediate availability of Konversation 1.0, a significant milestone in the lifetime of the Konversation project. This release includes major new functionality as well as a large amount of improvements to existing functionality, with an emphasis on user interface polish and overall reliability. Notable new features include a vertical treelist of tabs as an alternative to the traditional tab bar, auto-replacement of words in incoming and outgoing messages, an improved Channel Settings dialog now featuring a ban list, an optional expanding input box and many improvements to both DCC file transfers and DCC chats. Enjoy!


Changes in 1.0:
 User Interface 
 
+ It is now possible to place the tabs on the left side of the application window. This has been implemented as a treelist of tabs. The treelist supports all of the cosmetic and interactive properties of the original horizontal tab bar, including colored notifications, LED icons, (hover) close buttons with delayed activation, reordering, drag'n'drop and mouse wheel cruising. And a few tricks of its own. 
+ Connection status tabs now feature specific context menu entries to disconnect and reconnect, as well as to join a channel on that connection. 
+ The automatic resizing of tabs in the tab bar first implemented in version 0.19 is now optional. 
+ The grouping behavior of Channel List and Raw Log tabs in the tab bar has been improved. 
+ Disabling notification for a tab will now unset the active notification. 
+ The enabled/disabled state of the notifications for connection status tabs will now be remembered across sessions for configured networks. 
+ The speed of switching tabs in quick succession with the auto-spellchecking preference enabled has been improved. 
+ Using custom fonts in the user interface is now optional. The font used by the tab bar is now configurable. 
+ Events in the connection status tabs are now logged into separate logfiles. 
+ The Channel Settings dialog now includes a Ban List tab that allows viewing, adding and removing bans in a channel. The dialog can now be opened from the menu bar and chat window context menu in addition to the button in the topic area. 
+ Mode and topic handling in the Channel Settings dialog and the channel mode buttons have been overhauled to make them more robust and reliable. 
+ The number of Quick Buttons to show below the nickname list in channel tabs is now configurable. Additional buttons may be added or existing buttons removed. 
+ Konversation now supports auto-replacing words in incoming and outgoing messages. Regular expressions are supported. The auto-replace configuration can be found in the preferences dialog. The static Wiki link feature found in older versions has been retired in favor of an auto-replace rule. 
+ The search bar has been redesigned to provide a better user experience. 
+ The "Find Next" action will now open the search bar when there is no active search, matching the behavior of Konqueror and other KDE applications. 
+ The sorting of the nick completion list has been improved to put the last active user for a given completion prefix at the beginning of the list. 
+ The tab completion of the user's own nickname has been reenabled. 
+ The nick completion feature has been significantly cleaned up and made more reliable. A bug that could lead to an application crash during nick completion has been fixed. 
+ An option to expand the vertical size of the input box automatically when the text entered grows beyond the length of a single line has been added. 
+ The behavior of the input box on pasting text including leading or trailing newline characters has been improved never to cause lines being sent without user acknowledgement. 
+ The input box of connection status, channel and query tabs will now be disabled and the nickname list of channel tabs cleared when the respective server connection is closed. 
+ Konversation can now optionally insert a remember line whenever a tab is hidden, either by switching to a different tab or minimizing the window. 
+ Multiple consecutive remember lines will no longer be inserted. 
+ Remember lines can now also be inserted into the chat windows of connection status and DCC Chat tabs. 
+ The Colored Nicknames feature will now always assign the same color to the same nickname. 
+ The number of backlog lines to show in the chat window is now configurable. 
+ The recognition of URLs in the chat window has been improved to cope better with URLs containing or being surrounded by parenthesis and to exclude trailing dots and commas. 
+ Channel links following mode characters or surrounded by interpunctuation are now properly recognized in the chat window. 
+ The context menus for URLs and channel links in the topic area now match the context menus in the chat window. 
+ Multiple ignore or unignore actions ordered at the same time will no longer be shown on separate lines in the chat window. 
+ The nickname context menus in the chat window, topic are and the nickname list will now show "Ignore", "Unignore" and "Add to Watched Nicknames" entries as applicable. 
+ A bug that could lead to the chat window nickname context menu actions ceasing to function after the targeted user left the channel has been fixed. 
+ The Server List dialog now allows connecting to a specific server in a network even when a connection to that network has been previously established. If that connection is active, a dialog box will verify whether to disconnect from the current server and connect to the chosen one instead, otherwise the connection will simply be reestablished using the newly chosen server. 
+ The Quick Connect feature will now properly warn when the identity to be used in the connection attempt is not set up properly. 
+ The appearance and behavior of the warning about an incorrectly set-up identity have been improved. A prior connection attempt will now be automatically resumed after the identity settings have been corrected. 
+ Many of the pages in the Konversation preferences dialog have been redesigned and rewritten for improved consistency, reliability and clarity. The general layout of the dialog has been improved as well. 
+ The naming of certain actions in the Configure Shortcuts dialog has been improved to make them easier to recognize outside of their normal context in the application interface. 
+ Numerous improvements to keyboard navigation have been made. 
+ The nickname list now longer allows drag drop of channel or user links from the chat window onto list entries, as a DCC transfer of those data sources cannot succeed. 
+ The preference to show or hide the real names of users in the nickname list will now be applied immediately. 
+ The columns of the nickname list will no longer resize erratically when the preferences to show or hide real names and hostmasks are changed at runtime. 
+ A bug that could lead to nicknames being sent as messages when double-clicking a selection of multiple nicknames in the nickname list has been fixed. 
+ The placement of actions in the application menus has been improved. 
+ The shown/hidden state of the application menubar will now be remembered across sessions. When the menubar is hidden, a menu action to show it again will now be added to the chat window context menu. 
+ The "Close All Open Queries" menu action is now be disabled properly when there are no open queries. 
+ A bug that could lead to the "Close All Open Queries" menu action failing to properly close all open queries has been fixed. 
+ A bug that could lead to an application crash when closing the tab after choosing to ignore someone in a query has been fixed. 
+ The "Hide Nicklist" menu action will now be disabled properly when the tab shown does not have a nickname list has been fixed. 
+ The actions in the "Insert" menu will now be disabled properly when the current tab does not support them. 
+ The default double-click action in the "Watched Nicks Online" tab is now to open a query to the respective contact. 
+ The sorting in the Watched Nicks Online tab has been improved: Offline users are now always sorted at the bottom. 
+ Several bugs in the "Watched Nicknames Online" tab that could lead to application crashes have been fixed. 
+ Several errors in the chat window status messages produced by the Watched Nicks Online system have been corrected. 
+ A bug that could lead the the columns in the "Watched Nicks Online" list resizing erratically has been fixed. 
+ A bug that could lead to the status bar not being cleared properly when the last tab was closed or the application window lost focus after a link was launched from the chat window has been fixed. 
+ The display of temporary and static info texts in the status bar has been improved not to interfere with each other and provide more useful information. Also, the status bar lag info section is now updated more consistently to avoid jumping around of the other status bar sections. 
+ A bug that could lead to a wrong nickname count being shown in the status bar of channel tabs has been fixed. 
+ Repeated triggering of the "Open URL Catcher" menu action will now properly show and hide the URL Catcher tab. 
+ The warning about pasting text with multiple lines can now be properly disabled and reenabled from the Warning Dialogs preferences page. 
+ A bug that could lead to IRC bookmarks showing up as actions in the Configure Shortcuts dialog has been fixed. 
+ A bug that could lead to changes of the global KDE icon set not being applied to the tab bar close buttons immediately has been fixed. 
+ A bug that caused the application window to change its horizontal size after opening a Query to a user with a very long hostmask has been fixed. The DCC Chat and query tabs now use the same heading style as channel tabs. 
+ A bug that could lead to the topic and nickname list areas not keeping their size properly across tab switches has been fixed. 
+ A bug that could lead to certain types of KNotify event notifications not being executed properly when the system tray icon was enabled has been fixed. 
+ A bug that could lead to ampersands in network names being shown as underscores in the menu under certain circumstances has been fixed. 
+ A bug that could lead to an application crash when trying to access non-existing tabs via the Alt+number keyboard shortcuts has been fixed. 
+ A bug that could lead to the toolbar being hidden after it was edited has been fixed. 
+ A bug that could lead to the chat window context menu not being cancelled properly when clicking outside of it has been fixed. 
+ A bug that could lead to heavy disk seeking when the splitters separating the topic area and the nickname list from the chat window were moved has been fixed. 
+ The option to only show the application in the system tray at all times has been retired in favor of the standard KDE mechanic of minimizing into the system tray. 
 
 Commands 
 
+ The 'Now Playing` script invoked via the /media command alias now features support for XMMS and KSCD as well as improved support for untagged media files playing in Amarok. Support for non-ASCII encodings in file names and meta tags has been improved as well. 
+ New "/hop" and "/dehop" commands to grant or remove half-op status from a user have been added. 
+ A new "/devoice" command has been added. 
+ A new "/kickban" command to ban and immediately kick a user has been added. It supports the same parameters as the "/ban" command plus an additional "kickban reason" parameter. 
+ Commands to grant or remove status for users will now be applied to the user's own nickname when no nickname parameter is given. 
+ The "/unignore" command now supports the same simple nickname-only format as the "/ignore" command. 
+ If given no parameter, the "/away" command will now set the away state with the default away message. The "/back" and "/unaway" commands can be used to unset the away state. 
+ You may now use "%nick" as a placeholder for your own nickname in the auto- connect commands for a network. 
+ A bug that could lead to the auto-connect commands for a network not being executed correctly has been fixed. 
 
DCC
 
+ DCC file transfers now support file names containing spaces on send, receive and resume. The automatic replacement of spaces with underscores in file names can now be optionally disabled in the DCC preferences. 
+ File names are no longer being needlessly lower-cased during DCC transfers. 
+ The DCC file transfer and DCC Chat info messages shown in the chat window have been significantly improved to provide more useful information while being less excessively verbose. 
+ DCC Chats will now be logged properly. 
+ It is now possible to select multiple files in the DCC Status tab. 
+ The default size of the buffer used in DCC transfers has been increased to 8192kb for improved DCC performance. 
+ The DCC Status tab will no longer show a speed of '?' for completed, failed or aborted transfers. 
+ Bugs that could lead to the IP used for DCC transfers not being retrieved from the server correctly upon reconnect or in general on certain servers have been fixed. 
+ A bug that could lead to the progress bar of a transfer in the DCC Status tab being rendered at a wrong position has been fixed. 
+ A bug that could lead to Konversation's CPU usage spiking to 100% after a DCC Chat was closed from the remote side has been fixed. 
+ Several bugs that could lead to application crashes in DCC Chat tabs after the server connection from which the DCC Chat originated was closed have been fixed. 
 
Technology
 
+ Konversation will now properly split up very long lines into multiple messages by calculating the length of the message preamble and the number of bytes of the text payload. Encodings that use multiple or variable numbers of bytes per character are accounted for. 
+ The Disconnect and Reconnect menu actions and the respective input box commands have been rewritten for increased reliability. Their state will now be updated properly, and they will quit the IRC server in the correct manner. 
+ A previous away state will now be recreated upon reconnection to a server. 
+ The Quick Connect feature will now no longer join the auto-join channels of the configured network that the quick connect server was recognized as being a part of. 
+ The "Konversation" and "KonvDCOPIdentity" DCOP objects have been renamed to "irc" and "identity", respectively. Several bugs in the DCOP API have been fixed, and deprecated interfaces removed. 
+ Processing of the user lists of newly joined channels has been rewritten to fix several bugs, including improved compatibility with the Bip IRC proxy and other servers. 
+ The lag calculation and timeout handling code has been rewritten for improved reliability and performance. 
+ Recognition of the half-op user status has been improved. 
+ The detection of text being typed into the input box to prevent focussing new tabs at inconvenient times has been improved to work correctly with non-ASCII characters. 
+ The handling of channel user limits in the Channel Settings dialog is now more reliable. 
+ Support for mode flags encountered on UnrealIRCD servers has been improved. 
+ Support for RPL_DATASTR on UnrealIRCD servers has been improved. 
+ A bug that could lead to the mode flags displayed for users not being updated properly after they were kicked from a channel has been fixed. 
+ A bug that could lead to iterating over a configured network's servers failing after a connection failure has been fixed. 
+ A bug that could lead to Konversation connecting to the wrong server in a network when choosing to connect to a specific server from the Server List dialog has been fixed. 
+ A dialog will now ask the user for an additional nickname when all nicknames configured in the identity where tried unsuccessfully during a connection attempt. This replaces the previous behavior of repeatedly appending underscores to the last nickname, which eventually ran into the nickname length limit on the server. 
+ A bug that could lead to unnecessary nick changes immediately after connecting to a server has been fixed. 
+ A bug that could lead to Konversation trying to auto-identify multiple times upon connect on certain servers has been fixed. 
+ A bug that could lead to Konversation not picking up on users leaving a channel without providing a part or quit message on UnrealIRCD servers has been fixed. 
+ Changes to the list of auto-join channels for a network will now be applied immediately. 
+ The auto-reconnect preference will now be properly applied at runtime. 
+ Konversation will no longer enable IDENTIFY-MSG mode on servers that support it, but continues to be able to process messages with IDENTIFY-MSG prefixes in case an involved IRC proxy chose to enable IDENTIFY-MSG mode. 
+ The broken default for the Custom Web Browser preference has been fixed. 
+ Konversation will no longer allow Konsole tabs to be opened in KDE environments in which the use of terminals is prohibited by the KIOSK framework. 
+ A bug that could lead to an application crash when a Konsole tab was closed from the Konsole component's context menu has been fixed. 
+ A bug that could lead to an application crash when a channel was joined while the application window was hidden has been fixed. 
 
