---
title: Konversation 0.18 has been released!
date: 2005-07-06
layout: post
---
Changes in 0.18:
	
+ All nicks were blue when colored nicks are disabled with some setups
+ Cycle now works as expected
+ Gauge script was not working correctly when given an argument greater than 100
+ Mail script has been added
+ Button to invoke Regular Expression Editor (if installed) in Settings -> Highlight
+ Complete command line argument system for connection
+ An option to disable clickable nicks. Add ClickableNicks=false to konversationrc to disable it
+ Fixed a big memory leak in message processing
+ Nicklist slider now correctly resizes in all channels when it's resized and correctly restores on startup
+ `[[foo]]` is now a link to http://en.wikipedia.org/wiki/foo

